import React from "react"
import { connect } from 'react-redux'
import { setCouponEmpty } from "../actions/couponActions"
import { setCouponError } from "../actions/couponActions"
import { setCouponSuccess } from "../actions/couponActions"

@connect((store) => {
  return {
    coupon: store.coupon,
  };
})

export default class PaymentInput extends React.Component {

  handleChange(e) {
    if (e.target.value != '') {
      this.setCouponSuccess()
    } else {
      this.setCouponEmpty()
    }
  }

  render() {
    let className = this.props.coupon.status
    if (this.props.className) {
      className += ' '+this.props.className
    }

    let error;
    if (this.props.error) {
      error = (
        <p className="form-error">{this.props.error}</p>
      )
    }
    return <label className={className}>
      <input type="text" className="form-input" onChange={this.handleChange.bind(this)} />
      <p className="form-label">{this.props.label}</p>
      { error }
    </label>
  }
}