import React from "react"

export default class BillRules extends React.Component {
  render() {
    return <p className="bill-rules">
      <img src="https://cdn.scentbird.com/assets/content/payment-page/rules@2x-95e06b3c71b6234f3bc60f3cf77e155a.png"/>
      <span>You will receive an email confirmation when recipient accepts your gift. Scentbird ships between the 15th and the 18th of every month. Recipient will receive an email confirmation of shipment every month. Please allow 5-7 days for delivery.</span>
    </p>
  }
}