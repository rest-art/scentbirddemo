import React from "react"
import { connect } from "react-redux"

import PaymentInput from "../components/PaymentInput"
import { changeValue } from "../actions/shippingActions"
import { changeStatus } from "../actions/shippingActions"

import { changeBillingValue } from "../actions/billingActions"
import { changeBillingStatus } from "../actions/billingActions"

@connect((store) => {
  return {
    shipping: store.shipping,
    billing: store.billing,
  };
})

export default class ShippingAddress extends React.Component {

  change(field, e) {
    const value = e.target.value
    let status = "empty"
    let valid = false
    let required = true

    switch (field) {
      case "firstName": {
        // min 3 chars A-z
        valid = /^[a-zA-Z]{3,}/.test(value)
        break
      }
      case "lastName": {
        // min 3 chars A-z
        valid = /^[a-zA-Z]{3,}/.test(value)
        break
      }
      case "streetAddress": {
        // min 10 chars
        valid = /^.{10,}$/.test(value)
        break
      }
      case "apt": {
        required = false
        valid = value
        break
      }
      case "zip": {
        // US ZIP-code
        valid = /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(value)
        break
      }
      case "city": {
        // US City Name
        valid = /^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/.test(value)
        break
      }
      case "state": {
        // US State
        valid = /^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/.test(value)
        break
      }
      case "phone": {
        // phone
        required = false
        valid = /^([0|\+[0-9]{1,5})?([0-9]{10})$/.test(value)
        break
      }
    }

    if (valid) {
      status = "success"
    } else {
      if (required) {
        status = "error"

        if (!value.length) {
          status += " empty"
        }
      } else {
        status = !value.length ? "empty" : "error"
      }
    }

    if (this.props.data == 'shipping') {
      this.props.dispatch(changeValue(field, value))
      this.props.dispatch(changeStatus(field, status))
    }
    if (this.props.data == 'billing') {
      this.props.dispatch(changeBillingValue(field, value))
      this.props.dispatch(changeBillingStatus(field, status))
    }
  }

  render() {

    const shipping = this.props[this.props.data]

    return <div>
        
        <p className="title">{ this.props.title }</p>

        <div className="payment-flex">
          <PaymentInput
            label="First name"
            className="flex-half"
            status={shipping.firstName.status}
            value={shipping.firstName.value}
            onBlur={this.change.bind(this, 'firstName')}
          />
          <PaymentInput
            label="Last name"
            className="flex-half"
            status={shipping.lastName.status}
            value={shipping.lastName.value}
            onBlur={this.change.bind(this, 'lastName')}
          />
        </div>

        <div className="payment-flex">
          <PaymentInput
            label="Street address"
            className="flex-sixty"
            status={shipping.streetAddress.status}
            value={shipping.streetAddress.value}
            onBlur={this.change.bind(this, 'streetAddress')}
          />
          <PaymentInput
            label="Apt/Suit (Optional)"
            className="flex-thrd"
            status={shipping.apt.status}
            value={shipping.apt.value}
            onBlur={this.change.bind(this, 'apt')}
          />
        </div>

        <div className="payment-flex">

          <PaymentInput
            label="ZIP"
            className="flex-thrd"
            unrequired="true"
            status={shipping.zip.status}
            value={shipping.zip.value}
            onBlur={this.change.bind(this, 'zip')}
          />
          <PaymentInput
            label="City"
            className="flex-thrd"
            unrequired="true"
            status={shipping.city.status}
            value={shipping.city.value}
            onBlur={this.change.bind(this, 'city')}
          />
          <PaymentInput
            label="State"
            className="flex-thrd"
            unrequired="true"
            status={shipping.state.status}
            value={shipping.state.value}
            onBlur={this.change.bind(this, 'state')}
          />

        </div>

        <div className="payment-flex">
          <label className="empty disabled">
            <input type="text" className="form-input" disabled></input>
            <p className="form-label">United States</p> 
          </label>
        </div>

        <div className="payment-flex">
          <PaymentInput
            label="Mobile number (Optional)"
            error="Wrong number"
            className="flex-half"
            unrequired="true"
            status={shipping.phone.status}
            value={shipping.phone.value}
            onBlur={this.change.bind(this, 'phone')}
          />
          <label className="flex-half text">
            We may send you special discounts and offers
          </label>
        </div>
    </div>
  }
}