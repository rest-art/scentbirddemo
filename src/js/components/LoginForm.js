import React from "react"
import { connect } from "react-redux"

import PaymentInput from "../components/PaymentInput"
import { changeValue } from "../actions/loginActions"
import { changeStatus } from "../actions/loginActions"

@connect((store) => {
  return {
    login: store.login,
  };
})

export default class LogoSection extends React.Component {
  change(field, e) {
    const value = e.target.value
    let status = "empty"
    let valid = false
    let required = true

    switch (field) {
      case "email": {
        // Email regEx
        let regEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        valid = regEx.test(value)
        break
      }
      case "password": {
        // Min 10 chars
        valid = /^.{10,}$/.test(value)
        break
      }
    }

    if (valid) {
      status = "success"
    } else {
      if (required) {
        status = "error"
        
        if (!value.length) {
          status += " empty"
        }
      }
    }

    this.props.dispatch(changeValue(field, value))
    this.props.dispatch(changeStatus(field, status))
  }

  render() {
    const login = this.props.login;
    
    return <div className="payment-flex login-form">
      <PaymentInput
          label="Email address"
          className="flex-half"
          status={login.email.status}
          onBlur={this.change.bind(this, 'email')}
        />
      <PaymentInput
          label="Password"
          className="flex-half"
          type="password"
          status={login.password.status}
          onBlur={this.change.bind(this, 'password')}
        />
    </div>
  }
}
