import React from "react"
import { connect } from "react-redux"

import ShippingAddress from "../components/ShippingAddress"
import { toggleCheckbox } from "../actions/billingActions"

@connect((store) => {
  return {
    billing: store.billing,
  };
})

export default class BillingAddress extends React.Component {

  toggleCheckbox() {
    this.props.dispatch(toggleCheckbox(!this.props.billing.equal))
  }

  render() {
    const billing = this.props.billing.equal ? '' : (<ShippingAddress title="Billing Address" data="billing"/>)

    return <div>
        <label className="payment-text">
          <input type="checkbox" className="checkbox" id="cb2"
            checked={this.props.billing.equal}
            onChange={this.toggleCheckbox.bind(this)}
          />
          <label for="cb2" className="cb"></label>
          Use this address as my billing address
        </label>
    
    { billing }

    </div>
  }
}