import React from "react"
import { connect } from 'react-redux'
import * as Login from "../actions/loginActions"
import * as Secure from "../actions/secureActions"
import * as Shipping from "../actions/shippingActions"
import * as Billing from "../actions/billingActions"

@connect((store) => {
  return {
    login: store.login,
    secure: store.secure,
    shipping: store.shipping,
    billing: store.billing,
  };
})

export default class Buttons extends React.Component {
  validate() {
    const fields = [
      {
        name: 'login',
        data: this.props.login,
      },
      {
        name: 'secure',
        data: this.props.secure,
      },
      {
        name: 'shipping',
        data: this.props.shipping,
      },
    ]

    if (!this.props.billing.equal) {
      fields.push({
        name: 'billing',
        data: this.props.billing,
      })
    }

    const badFields = fields.map(input => {
      const field = input.data
      let errors = []

      for (let prop in field) {
        let ignored = ['equal', 'apt', 'type', 'country']
        if (field.hasOwnProperty(prop) && ignored.indexOf(prop) === -1) {
          if (field[prop].status == 'empty') {
            errors.push(prop)
          }
        }
      }

      return {
        name: input.name,
        data: errors
      }

    }).map(fields => {
      fields.data.map(field => {

        let status = 'error empty'

        if (fields.name == 'login') {
          this.props.dispatch(Login.changeStatus(field, status))
        }
        if (fields.name == 'secure') {
          this.props.dispatch(Secure.changeStatus(field, status))
        }
        if (fields.name == 'shipping') {
          this.props.dispatch(Shipping.changeStatus(field, status))
        }
        if (fields.name == 'billing') {
          this.props.dispatch(Billing.changeBillingStatus(field, status))
        }
      })
      return fields
    })
  }

  render() {
    return <div className="buttons">
      <a className="back">Back</a>&nbsp;
      <a className="button" onClick={this.validate.bind(this)}>BUY NOW</a>
    </div>
  }
}