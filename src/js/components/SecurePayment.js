import React from "react"
import { connect } from "react-redux"
import Media from 'react-media'

import PaymentInput from "../components/PaymentInput"
import { changeValue } from "../actions/secureActions"
import { changeStatus } from "../actions/secureActions"

@connect((store) => {
  return {
    secure: store.secure,
  };
})

export default class SecurePayment extends React.Component {

  change(field, e) {
    const value = e.target.value
    let status = "empty"
    let valid = false
    let required = true

    switch (field) {
      case "cc": {
        // Visa, MasterCard, American Express, Diners Club, Discover, and JCB cards matches regEx:
        const regEx = /^(?:4[0-9]{12}(?:[0-9]{3})?|(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|6(?:011|5[0-9]{2})[0-9]{12}|(?:2131|1800|35\d{3})\d{11})$/
        valid = regEx.test(value.replace(/\s|-/g, ''))
        break
      }
      case "cvc": {
        // 3-4 digits CVC code
        valid = (value != '111' && /^[0-9]{3,4}$/.test(value))
        break
      }
      case "month": {
        valid = value
        break
      }
      case "year": {
        valid = value
        break
      }
    }

    if (valid) {
      status = "success"
    } else {
      if (required) {
        status = "error"
        
        if (!value.length) {
          status += " empty"
        }
      }
    }

    this.props.dispatch(changeValue(field, value))
    this.props.dispatch(changeStatus(field, status))
  }

  render() {
    const secure = this.props.secure
    const date = (<div className="payment-flex">
      <label className={ "flex-twnt sel " + secure.month.status }>
        <select type="text" className="form-select" onChange={this.change.bind(this, 'month')}>
          <option value="">Month</option>
          <option value="1">01 - Jan</option>
          <option value="2">02 - Feb</option>
          <option value="3">03 - Mar</option>
          <option value="4">04 - Apr</option>
          <option value="5">05 - May</option>
          <option value="6">06 - Jun</option>
          <option value="7">07 - Jul</option>
          <option value="8">08 - Aug</option>
          <option value="9">09 - Sep</option>
          <option value="10">10 - Oct</option>
          <option value="11">11 - Nov</option>
          <option value="12">12 - Dec</option>
        </select>
        <i className="select-icon"></i>
        { secure.month.status == 'error empty' ? (<p className="form-error">This field is required</p>) : '' } 
      </label>

      <label className={ "flex-twnt sel " + secure.year.status }>
        <select type="text" className="form-select" onChange={this.change.bind(this, 'year')}>
          <option value="">Year</option>
          <option value="2016">2016</option>
          <option value="2017">2017</option>
          <option value="2018">2018</option>
          <option value="2019">2019</option>
          <option value="2020">2020</option>
          <option value="2021">2021</option>
          <option value="2022">2022</option>
          <option value="2023">2023</option>
          <option value="2024">2024</option>
          <option value="2025">2025</option>
        </select>
        <i className="select-icon"></i>
        { secure.year.status == 'error empty' ? (<p className="form-error">This field is required</p>) : '' } 
      </label>
      <label className="flex-fty empty">
        <p className="form-label">Exp.</p>  
      </label>
    </div>)

    return <div className="secure">
      <div className="text">
        <span>128-BIT ENCRYPTION. YOU'RE SAFE</span>
        <div className="icons"></div>
      </div>

      <div className="payment-flex cc-row">

        <PaymentInput
          label="Credit card number"
          className="flex-sixty payment-card"
          status={secure.cc.status}
          onBlur={this.change.bind(this, 'cc')}
        />

        <Media query="(max-width: 768px)">
          {date}
        </Media>

        <PaymentInput
          label="Security code"
          className="flex-thrd cvc"
          status={secure.cvc.status}
          onBlur={this.change.bind(this, 'cvc')}
        >
          <button className="payment-help">
            <div className="payment-tooltip">
              <i className="tooltip-close"></i>
              <div className="tooltip-content">
                <h5>What is a card security code?</h5>
                <p>A three- or four-digit code on your credit card. The location varies slightly depending on your type of card</p>
                <div className="tooltip-mc">
                  <h6>VISA, MasterCard</h6>
                  <p>Back of card</p>
                </div>
                <div className="tooltip-ae">
                  <h6>American Express</h6>
                  <p>Front of card</p>
                </div>
              </div>
            </div>
          </button>
        </PaymentInput>

      </div>

      <Media query="(min-width: 768px)">
        {date}
      </Media>
    </div>
  }
}