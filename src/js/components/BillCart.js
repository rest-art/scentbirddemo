import React from "react"
import { connect } from "react-redux"

import PaymentInput from "../components/CouponInput"
import Media from 'react-media'
import { toggleCheckbox } from "../actions/couponActions"
import { showCouponForm } from "../actions/couponActions"

@connect((store) => {
  return {
    coupon: store.coupon,
  };
})

export default class BillSection extends React.Component {

  toggleCheckbox() {
    this.props.dispatch(toggleCheckbox(!this.props.coupon.checked))
  }
  showCouponForm() {
    this.props.dispatch(showCouponForm())
  }

  render() {
    return <div className="bill-section-cart clearfix">
        <div className="bill-shipment no-perfume-selected"></div>

        <table className="bill-table bordered">
          <tbody>
            <tr>
              <td className="table-item">Monthly subscription</td>
              <td className="table-price">$14.95</td>
            </tr>
            <tr>
              <td className="table-item">Shipping</td>
              <td className="table-price">Free</td>
            </tr>
            <tr>
              <td className="table-item">Tax</td>
              <td className="table-price">$2.35</td>
            </tr>
            <tr className="table-discount">
              <td className="table-item">Discount</td>
              <td className="table-price">- $5</td>
            </tr>
            <tr className="table-credit">
              <td className="table-item">Credit (Balance $100)</td>
              <td className="table-price">$50.00 
                <input type="checkbox" className="checkbox" id="cb1"
                       onChange={this.toggleCheckbox.bind(this)}
                       checked={this.props.coupon.checked}/>
                <label for="cb1" className="cb white"></label>
              </td>
            </tr>
            <tr className="table-total">
              <td className="table-item">Total</td>
              <td className="table-price">$25.00</td>
            </tr>
          </tbody>
        </table>
        
        <div class="clearfix"/>
        
        <div className={this.props.coupon.active ? 'bill-coupon active' : 'bill-coupon'}>

          <form className="coupon-form">
            <div className="payment-flex">
              <PaymentInput label="Coupon"/>
              <button type="submit">Submit</button>
            </div>
          </form>

          <div className="text"> 
            Have a&nbsp;
            <span className="invite-mark" onClick={this.showCouponForm.bind(this)}>
              coupon code
            </span>
            ?
          </div>
        </div>
      </div>
  }
}