import React from "react"

export default class PaymentInput extends React.Component {

  render() {

    let type="text"
    if (this.props.type) {
      type = this.props.type
    }

    let className = this.props.status
    if (this.props.className) {
      className += ' '+this.props.className
    }

    let error;
    if (/^error/.test(this.props.status)) {
      let text = 'This field is required'

      if (this.props.error) {
        text = this.props.error;
      }

      error = (
        <p className="form-error">{text}</p>
      );
    }

    return <label className={className}>

      <input type={type}
        className="form-input"
        onBlur={this.props.onBlur}
        placeholder={this.props.value}
      />

      <p className="form-label">{this.props.label}</p>

      { error }

      { this.props.children }

    </label>
  }
}