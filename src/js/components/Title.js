import React from "react"

export default class Title extends React.Component {
  render() {
    return <div className="page-title">
      <h1 className="forms-h">{this.props.txt}</h1>
      <div className="highlight">Billed monthly. Renews automatically, cancel any time. Free shipping.</div>
    </div>
  }
}
