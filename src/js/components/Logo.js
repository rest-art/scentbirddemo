import React from "react"

export default class LogoSection extends React.Component {
  render() {
    return <div className="logo-section">
      <a className="payment-logo"/>
    </div>
  }
}