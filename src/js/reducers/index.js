import { combineReducers } from "redux"

import login from "./loginReducer"
import coupon from "./couponReducer"
import secure from "./secureReducer"
import shipping from "./shippingReducer"
import billing from "./billingReducer"

export default combineReducers({
  login,
  coupon,
  secure,
  shipping,
  billing
})
