export default function reducer(state={
      email: {
        value: '',
        status: 'empty'
      },
      password: {
        value: '',
        status: 'empty'
      },
      type: ''
    }, action) {

      if (/^LOGIN_CHANGE_VALUE_/.test(action.type)) {
          const theNew = {...state}
          theNew[action.payload.field].value = action.payload.value
          return theNew
      }

      if (/^LOGIN_CHANGE_STATUS_/.test(action.type)) {
          const theNew = {...state}
          theNew[action.payload.field].status = action.payload.status
          return theNew
      }

      if (action.type == 'SET_USER_TYPE') {
          return {...state, type: action.payload}
      }

    return state
}
