export default function reducer(state={
      firstName: {
        value: '',
        status: 'empty'
      },
      lastName: {
        value: '',
        status: 'empty'
      },
      streetAddress: {
        value: '',
        status: 'empty'
      },
      apt: {
        value: '',
        status: 'empty'
      },
      zip: {
        value: '',
        status: 'empty'
      },
      city: {
        value: '',
        status: 'empty'
      },
      state: {
        value: '',
        status: 'empty'
      },
      country: {
        value: 'United States',
        status: 'empty'
      },
      phone: {
        value: '',
        status: 'empty'
      },
    }, action) {

      if (/^SHIPPING_CHANGE_VALUE_/.test(action.type)) {
          const theNew = {...state}
          theNew[action.payload.field].value = action.payload.value
          return theNew
      }

      if (/^SHIPPING_CHANGE_STATUS_/.test(action.type)) {
          const theNew = {...state}
          theNew[action.payload.field].status = action.payload.status
          return theNew
      }

    return state
}
