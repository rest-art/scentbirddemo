export default function reducer(state={
      cc: {
        value: '',
        status: 'empty',
      },
      cvc: {
        value: '',
        status: 'empty',
      },
      month: {
        value: '',
        status: 'empty',
      },
      year: {
        value: '',
        status: 'empty',
      }
    }, action) {

      if (/^CC_CHANGE_VALUE_/.test(action.type)) {
          const theNew = {...state}
          theNew[action.payload.field].value = action.payload.value
          return theNew
      }

      if (/^CC_CHANGE_STATUS_/.test(action.type)) {
          const theNew = {...state}
          theNew[action.payload.field].status = action.payload.status
          return theNew
      }

    return state
}
