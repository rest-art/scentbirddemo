export default function reducer(state={
      equal: true,
      firstName: {
        value: '',
        status: 'empty'
      },
      lastName: {
        value: '',
        status: 'empty'
      },
      streetAddress: {
        value: '',
        status: 'empty'
      },
      apt: {
        value: '',
        status: 'empty'
      },
      zip: {
        value: '',
        status: 'empty'
      },
      city: {
        value: '',
        status: 'empty'
      },
      state: {
        value: '',
        status: 'empty'
      },
      country: {
        value: 'United States',
        status: 'empty'
      },
      phone: {
        value: '',
        status: 'empty'
      },
    }, action) {
  
      if (/^BILLING_CHANGE_VALUE_/.test(action.type)) {
          const theNew = {...state}
          theNew[action.payload.field].value = action.payload.value
          return theNew
      }

      if (/^BILLING_CHANGE_STATUS_/.test(action.type)) {
          const theNew = {...state}
          theNew[action.payload.field].status = action.payload.status
          return theNew
      }

      if (action.type == 'BILLING_TOGGLE_CHECKBOX') {
        return {...state, equal: action.payload}
      }

    return state
}
