export default function reducer(state={
      active: false,
      status: 'empty',
      checked: true,
    }, action) {

    switch (action.type) {
      case "SHOW_COUPON_FORM": {
        return {...state, active: true}
      }
      case "TOGGLE_CHECKBOX": {
        return {...state, checked: action.payload}
      }
      case "SET_COUPON_EMPTY": {
        return {...state, status: 'empty'}
      }
      case "SET_COUPON_SUCCESS": {
        return {...state, status: 'success'}
      }
      case "SET_COUPON_ERROR": {
        return {...state, status: 'error'}
      }
    }

    return state
}
