export function changeValue(field, value) {
  const type = "LOGIN_CHANGE_VALUE_"+field.toLocaleUpperCase()
  return {
    type,
    payload: {
      field,
      value,
    }
  }
}

export function changeStatus(field, status) {
  const type = "LOGIN_CHANGE_STATUS_"+field.toLocaleUpperCase()
  return {
    type,
    payload: {
      field,
      status,
    }
  }
}

export function setUserType(type) {
  return {
    type: 'SET_USER_TYPE',
    payload: type,
  }
}
