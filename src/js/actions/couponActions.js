export function showCouponForm() {
  return {
    type: 'SHOW_COUPON_FORM',
    payload: {
      active: true,
    },
  }
}

export function toggleCheckbox(checked) {
  return {
    type: 'TOGGLE_CHECKBOX',
    payload: checked,
  }
}

export function setCouponEmpty() {
  return {
    type: 'SET_COUPON_EMPTY',
    payload: {
      status: 'empty',
    },
  }
}

export function setCouponSuccess() {
  return {
    type: 'SET_COUPON_SUCCESS',
    payload: {
      status: 'success',
    },
  }
}

export function setCouponError() {
  return {
    type: 'SET_COUPON_ERROR',
    payload: {
      status: 'error',
    },
  }
}