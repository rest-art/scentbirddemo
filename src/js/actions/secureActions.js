export function changeValue(field, value) {
  const type = "CC_CHANGE_VALUE_"+field.toLocaleUpperCase()
  return {
    type,
    payload: {
      field,
      value,
    }
  }
}

export function changeStatus(field, status) {
  const type = "CC_CHANGE_STATUS_"+field.toLocaleUpperCase()
  return {
    type,
    payload: {
      field,
      status,
    }
  }
}
