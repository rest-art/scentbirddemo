export function toggleCheckbox(checked) {
  return {
    type: 'BILLING_TOGGLE_CHECKBOX',
    payload: checked,
  }
}

export function changeBillingValue(field, value) {
  const type = "BILLING_CHANGE_VALUE_"+field.toLocaleUpperCase()
  return {
    type,
    payload: {
      field,
      value,
    }
  }
}

export function changeBillingStatus(field, status) {
  const type = "BILLING_CHANGE_STATUS_"+field.toLocaleUpperCase()
  return {
    type,
    payload: {
      field,
      status,
    }
  }
}
