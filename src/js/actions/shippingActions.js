export function changeValue(field, value) {
  const type = "SHIPPING_CHANGE_VALUE_"+field.toLocaleUpperCase()
  return {
    type,
    payload: {
      field,
      value,
    }
  }
}

export function changeStatus(field, status) {
  const type = "SHIPPING_CHANGE_STATUS_"+field.toLocaleUpperCase()
  return {
    type,
    payload: {
      field,
      status,
    }
  }
}
