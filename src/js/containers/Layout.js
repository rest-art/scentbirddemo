import React from "react"
import { connect } from "react-redux"

import Logo from "../components/Logo"
import BillSection from "../containers/BillSection"
import FormsSection from "../containers/FormsSection"

import './../../less/main.less'

export default class Layout extends React.Component {

  render() {

    return <div className="container">

        <div class="payment-page">

          <Logo/>
          <BillSection/>
          <FormsSection/>

        </div>

    </div>
  }
}
