import React from "react"
import { connect } from "react-redux"

import BillRules from "../components/BillRules"
import BillCart from "../components/BillCart"
import Title from "../components/Title"
import Media from 'react-media'

export default class BillSection extends React.Component {

  render() {
    return <div className="bill-section">

      <Media query="(max-width: 1039px)">
        <Media query="(max-width: 768px)">
          {matches => matches ? (
            <Title txt="Monthly subscription"/>
          ) : (
            <Title txt="Month-to-month subscription"/>
          )}
        </Media>
      </Media>

      <BillCart />

      <Media query="(min-width: 1040px)">
        <BillRules/>
      </Media>
      
    </div>
  }
}
