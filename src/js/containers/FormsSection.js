import React from "react"
import { connect } from "react-redux"

import Title from "../components/Title"
import BillRules from "../components/BillRules"
import LoginForm from "../components/LoginForm"
import Buttons from "../components/Buttons"
import ShippingAddress from "../components/ShippingAddress"
import BillingAddress from "../components/BillingAddress"
import SecurePayment from "../components/SecurePayment"

import { setUserType } from "../actions/loginActions"

import Media from 'react-media'

@connect((store) => {
  return {
    shipping: store.shipping,
    login: store.login,
  };
})

export default class FormSection extends React.Component {

  setUserType(type) {
    this.props.dispatch(setUserType(type))
  }

  render() {

    return <div className="forms-section">

      <Media query="(min-width: 1040px)">
        <Title txt="Month-to-month subscription"/>
      </Media>

      <form className="payment-form">

        <Media query="(max-width: 1039px)">

          {matches => matches ? (
            
            <div>
              <p className="title">Choose your subscription type</p>
              <div className="mf">
                <div className="payment-flex">
                  <div>
                    <span onClick={this.setUserType.bind(this, 'female')} 
                      className={this.props.login.type == 'female' ? 'female picked' : 'female'} />
                      For Women
                  </div>
                  <div>
                    <span onClick={this.setUserType.bind(this, 'male')}
                      className={this.props.login.type == 'male' ? 'male picked' : 'male'} />
                      For Men
                  </div>
                </div>
                <div className="warning">Your info is required.</div>
              </div>
            </div>
          ) : (
            <p className="title">Create account</p>
          )}

        </Media>

        <LoginForm/>

        <Media query="(min-width: 1039px)">
          <div className="text-question">Have an account? <a>Log in</a></div>
        </Media>

        <ShippingAddress title="Shipping address" data="shipping" />

        <BillingAddress />        

        <p className="title">Secure credit card payment</p>

        <SecurePayment />

        <Buttons />
      </form>

      <Media query="(max-width: 1039px)">
        <BillRules/>
      </Media>

    </div>
  }
}